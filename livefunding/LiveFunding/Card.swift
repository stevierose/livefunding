//
//  Card.swift
//  LiveFunding
//
//  Created by Steven Roseman on 12/10/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import Foundation

class card {
    var ccNum:String!
    var redactedCCNum:String!
    var expMonth:Int!
    var expYear:Int!
    var cvvNum:Int!
    var event:String!
    var isCardTransaction:Bool = false
    var cardUsersName:String!
    var cardImg:UIImage!
    
    init(ccNum:String, redactedCCNum: String, expMonth: Int, expYear: Int, cvvNum: Int, event: String,
         isCardTransaction: Bool, cardUsersName: String, cardImg: UIImage) {
        self.ccNum = ccNum
        self.redactedCCNum = redactedCCNum
        self.expMonth = expMonth
        self.expYear = expYear
        self.cvvNum = cvvNum
        self.event = event
        self.isCardTransaction = isCardTransaction
        self.cardUsersName = cardUsersName
        self.cardImg = cardImg
        
        
    }
}

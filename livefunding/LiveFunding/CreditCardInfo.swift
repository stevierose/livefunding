//
//  CreditCardInfo.swift
//  LiveFunding
//
//  Created by Steven Roseman on 7/1/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import Foundation

class CreditCardInfo {
    
    var creditCardNum:String!
    var redactedCCNum:String!
    var expMonth:Int!
    var expYear:Int!
    var cvvNum:Int!
    var event:String!
    var isCardTransaction:Bool = false
    var cardUsersName:String!
    var cardImg:UIImage!
    
    
    
}

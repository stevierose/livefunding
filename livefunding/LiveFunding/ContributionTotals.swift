  //
//  ContributionTotals.swift
//  LiveFunding
//
//  Created by Steven Roseman on 7/8/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import Alamofire

class ContributionTotals: UIViewController, UITextFieldDelegate {

    @IBOutlet var contributorsLabel: UILabel!
    @IBOutlet var totalLabel: UILabel!
    @IBOutlet var dollarSIgnLabel: UILabel!
    @IBOutlet var eventNameView: UIView!
    @IBOutlet var eventNameLabel: UILabel!
    @IBOutlet var dollarAmountTotalLabel: UILabel!
    @IBOutlet var contributorsTotalLabel: UILabel!
    var ref:FIRDatabaseReference!
    var contributeRef:FIRDatabaseReference!
    var total:String!
    var contributionTotals:Double! = 0.0
    var totalString:String!
    var counterTotal:Int!
    var finalTotal:Int!
    var contributionStringTotal:String!
    var eventName:String!
    var phoneNumber:String!
    var API_KEY:String!
    var att:Any = "AT&T"
    @IBOutlet var newEventName: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        API_KEY = "5a7b2cb8a3c1459e195cab1781a2f881304ba9a7"
        finalTotal = 0
       //retrieve contribution totals
       checkTotal()
        newEventName.isHidden = true
        eventNameView.isHidden = true
        eventNameLabel.isHidden = true
        newEventName.delegate = self
        nav(name: eventName)
    }

    @IBAction func refreshButtonTapped(_ sender: Any) {
        checkTotal()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageView") as! LandingPageView
        
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        
    }
    
    func checkTotal(){
        ref = FIRDatabase.database().reference()
        print(eventName)
        
        ref.child(eventName).queryOrderedByKey().observe(.childAdded, with: {
            snapshot in
            
            for (key,value) in (snapshot.value as? [String:AnyObject])!{
                
                
                if key == "contributon"{
                    self.totalString = value as! String
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .currency
                    let currencySymbol = "$"
                    
                    self.total = currencySymbol + (value as! String)
                   
//                    if let number = formatter.number(from: self.total){
//                        let amount = (number.decimalValue as NSDecimalNumber).doubleValue
//
//                        self.contributionTotals = self.contributionTotals + amount
//
//                    }
                    
                } else if key == "counter"{
                    
                    self.counterTotal = value as? Int
                    self.finalTotal = self.finalTotal + Int(self.counterTotal)
                   
                    
                }
            }
            
            self.contributionStringTotal = String(self.total)
           
            if self.contributionStringTotal.characters.count == 7{
                   self.contributionStringTotal.insert(",", at: self.contributionStringTotal.index(self.contributionStringTotal.startIndex, offsetBy: 1))
                
            } else if self.contributionStringTotal.characters.count == 8{
                self.contributionStringTotal.insert(",", at: self.contributionStringTotal.index(self.contributionStringTotal.startIndex, offsetBy: 2))
               
            } else if self.contributionStringTotal.characters.count == 9{
                self.contributionStringTotal.insert(",", at: self.contributionStringTotal.index(self.contributionStringTotal.startIndex, offsetBy: 3))
                
            }
            self.contributorsTotalLabel.text = String(self.finalTotal)
            self.dollarAmountTotalLabel.text = self.contributionStringTotal
        })
        self.counterTotal = 0
    }
    
    @IBAction func addAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: "Choose Option", preferredStyle: UIAlertControllerStyle.actionSheet)
        let alertAction = UIAlertAction(title: "Send Thank you note", style: UIAlertActionStyle.default) { (action) in
            self.sendThankYouMessage()
            
        }
        let addContributor = UIAlertAction(title: "Add Event", style: UIAlertActionStyle.default) { (action) in
            
            self.newEventName.isHidden = false
            self.eventNameLabel.isHidden = false
            self.eventNameView.isHidden = false
            self.dollarAmountTotalLabel.isHidden = true
            self.contributorsTotalLabel.isHidden = true
            self.contributorsLabel.isHidden = true
            self.dollarSIgnLabel.isHidden = true
            self.totalLabel.isHidden = true
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (action) in
            
        }
        alertController.addAction(addContributor)
        alertController.addAction(alertAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)

        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let name = newEventName.text
        newEventName.resignFirstResponder()
       
        if let newName = name{
            addContributionEvent(newEvent:newName)
        } else {
            return false
        }
   
        
        return true
    }
    func addContributionEvent(newEvent:String) {
        ref = FIRDatabase.database().reference().child("contributionEvents")
        let key = ref.childByAutoId().key
        
        
        let event = ["id":key,
                     "eventName": newEvent,
                     ]
        
        ref.child(key).setValue(event)
        
        self.newEventName.isHidden = true
        self.eventNameLabel.isHidden = true
        self.eventNameView.isHidden = true
        
        self.dollarAmountTotalLabel.isHidden = false
        self.contributorsTotalLabel.isHidden = false
        self.contributorsLabel.isHidden = false
        self.dollarSIgnLabel.isHidden = false
        self.totalLabel.isHidden = false
    }

    //compose email with all carriers
    //At&t-conumer cellular-trac phone wirelessnumber@txt.att.net
    //Sprint wirelessnumber@messaging.sprintpcs.com
    //Verizon-Straight Talk-Xfinity wirelessnumber@vtext.com
    //Tmobile wirelessnumber@tmomail.net
    //Cricket wirelessnumber@sms.mycricket.com
    //MetroPCS wirelsessnumber@mymetropcs.com
    //US Cellular wirelessnumber@email.uscc.net
    

    func sendThankYouMessage(){
        ref = FIRDatabase.database().reference()
        ref.child(eventName).queryOrderedByKey().observe(.childAdded, with: { (snapshot) in
            for (key,value) in (snapshot.value as? [String:AnyObject])!{
                if key == "contributorPhoneNum"{
                    self.phoneNumber = value as! String
                    let carrierLookUp = "http://www.carrierlookup.com/index.php/api/lookup?key=" + self.API_KEY + "&number=" + self.phoneNumber + ""
                    
                    Alamofire.request(carrierLookUp).responseJSON { (response) in
                        for cn in (response.result.value as? [String:Any])!{
                            print(cn)
                        }
                    }
                    
                }
            }
        })
    }
}

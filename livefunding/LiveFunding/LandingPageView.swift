//
//  LandingPageView.swift
//  LiveFunding
//
//  Created by Steven Roseman on 6/21/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import UIKit
import Firebase
import Stripe





class LandingPageView: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, CardIOPaymentViewControllerDelegate{

    
    @IBOutlet var passwordBgView: UIView!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var passwordSubmitButton: UIButton!
    var dataForPicker = [String]()
    var eventChosen:String!
    @IBOutlet var pickerView: UIPickerView!
    var creditInfoArray = [CreditCardInfo]()
    var ref: FIRDatabaseReference!
    var refHandle: UInt!
    var isCash:Bool!
    @IBOutlet var ccButton: UIButton!
    @IBOutlet var cButton: UIButton!
    @IBOutlet var ctButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var passwordIncorrectLabel: UILabel!
    
    @IBOutlet var usernameTextfield: UITextField!
    
    override func loadView() {
        super.loadView()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        //Load event
        ref = FIRDatabase.database().reference().child("contributionEvents")
        ref.queryOrderedByKey().observe(.childAdded, with: {(snapshot) in
            for (key, value) in (snapshot.value as? [String:AnyObject])!{
                if key == "eventName"{
                    print(value)
                    
                    self.dataForPicker.append(value as! String)
                    
                    self.pickerView.reloadAllComponents()
                }
            }
            
        })
        

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //addContributionEvent()
        addButtonDimensions()
         nav(name: "Welcome to HBCU Live Funding")
       
        pickerView.tintColor = UIColor.groupTableViewBackground
    
        passwordBgView.isHidden = true
        passwordTextField.isHidden = true
        passwordSubmitButton.isHidden = true
        cancelButton.isHidden = true
        activityIndicator.isHidden = true
        passwordIncorrectLabel.isHidden = true
        
        eventChosen = "Choose an event"
       
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataForPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataForPicker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        eventChosen = dataForPicker[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = dataForPicker[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 15.0)!,NSForegroundColorAttributeName:UIColor.groupTableViewBackground])
        return myTitle
    }
    
    @IBAction func ccButtonTapped(_ sender: Any) {
        //stop user from scanning cc unitl they choose an event
       
        if eventChosen == "Choose an event"{
            //alert user they must select an event
            noEventChosenAlert()
            
        } else {
            let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
            cardIOVC?.collectCardholderName = true
            cardIOVC?.hideCardIOLogo = true
            cardIOVC?.modalPresentationStyle = .formSheet
            
            present(cardIOVC!, animated: true, completion: nil)
        }
       
    }
    
    @IBAction func cashButtonTapped(_ sender: Any) {
        
        if eventChosen == "Choose an event"{
            //alert no event chosen
            noEventChosenAlert()
        } else {
            passwordBgView.isHidden = false
            passwordTextField.isHidden = false
            passwordSubmitButton.isHidden = false
            cancelButton.isHidden = false
            //hide buttons and image
            ccButton.isHidden = true
            cButton.isHidden = true
            ctButton.isHidden = true
            
            //hide pickerView
            pickerView.isHidden = true
            
            isCash = true
 
        }

    }
    
    @IBAction func totalsButtonTapped(_ sender: Any) {
        
        passwordBgView.isHidden = false
        passwordTextField.isHidden = false
        passwordSubmitButton.isHidden = false
        cancelButton.isHidden = false
        //hide buttons and image
        ccButton.isHidden = true
        cButton.isHidden = true
        ctButton.isHidden = true
        
        
        isCash = false
      
    }
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            let ccInfo = CreditCardInfo()
            //card image

            ccInfo.cardImg = info.cardImage
            ccInfo.creditCardNum = info.cardNumber
            ccInfo.redactedCCNum = info.redactedCardNumber
            ccInfo.expMonth = Int(info.expiryMonth)
            ccInfo.expYear = Int(info.expiryYear)
            ccInfo.cvvNum = Int(info.cvv)
            ccInfo.event = eventChosen
            ccInfo.isCardTransaction = true
            ccInfo.cardUsersName = info.cardholderName
            creditInfoArray.append(ccInfo)
            
            let controller = self.storyboard!.instantiateViewController(withIdentifier: "CashContributionView") as! CashContributionView
            controller.cardDetails = creditInfoArray
            
            let controllerNav = UINavigationController(rootViewController: controller)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            
            
            appDelegate.window?.rootViewController = controllerNav
        }
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        

    
        guard let password = passwordTextField.text else {
            return
        }
        
        if isCash{
            FIRAuth.auth()?.signIn(withEmail: "test1@gmail.com", password: password, completion: { (user, error) in
                
                if error != nil {
                    
                    self.passwordIncorrectLabel.isHidden = false
                    self.dismissKeyboard()
                    self.passwordTextField.text = ""
                    return

                }

                print("success")
                //go to cash contribution page
                //set progress dialog
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
                
                //check saved password
                let ccInfo = CreditCardInfo()
                ccInfo.isCardTransaction = false
                ccInfo.event = self.eventChosen
                self.creditInfoArray.append(ccInfo)
                
                let controller = self.storyboard!.instantiateViewController(withIdentifier: "CashContributionView") as! CashContributionView
                controller.cardDetails = self.creditInfoArray
                
                let controllerNav = UINavigationController(rootViewController: controller)
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                
                
                appDelegate.window?.rootViewController = controllerNav
                
                
            })

        } else {
            FIRAuth.auth()?.signIn(withEmail: "contributiontotal@gmail.com", password: password, completion: { (user, error) in
                
                if error != nil {
                    print(error!.localizedDescription)
                    
                    self.passwordIncorrectLabel.isHidden = false
                    //self.dismissKeyboard()
                    self.passwordTextField.text = ""
//                    self.passwordBgView.isHidden = true
//                    self.passwordSubmitButton.isHidden = true
//                    self.pickerView.isHidden = false
//                    self.ccButton.isHidden = false
//                    self.cButton.isHidden = false
//                    self.ctButton.isHidden = false
//                    self.eventChosen = "Choose"
//                    self.pickerView.reloadComponent(0)
                    
                    
                    return
                }
                
                //success
                //go to contribution total page
                
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContributionTotals") as! ContributionTotals
                controller.eventName = self.eventChosen
                
                let controllerNav = UINavigationController(rootViewController: controller)
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.window?.rootViewController = controllerNav
            })

        }

        
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        passwordBgView.isHidden = true
        passwordTextField.isHidden = true
        passwordSubmitButton.isHidden = true
        cancelButton.isHidden = true
        //hide buttons and image
        ccButton.isHidden = false
        cButton.isHidden = false
        ctButton.isHidden = false
        
        //hide pickerView
        pickerView.isHidden = false

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageView") as! LandingPageView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
    }
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        print("cancelled")
        paymentViewController?.dismiss(animated: true, completion: nil)
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageView") as! LandingPageView
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
    }
    
    func noEventChosenAlert(){
        let alertController = UIAlertController(title: "Oh no..You forgot", message: "Choose an event from above", preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
            
        }
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    func addButtonDimensions(){
        submitButton.layer.cornerRadius = 5
        submitButton.layer.borderWidth = 3
        submitButton.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        cancelButton.layer.cornerRadius = 5
        cancelButton.layer.borderWidth = 3
        cancelButton.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        passwordBgView.layer.cornerRadius = 5
        passwordBgView.layer.borderWidth = 3
        passwordBgView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        //ccButton.backgroundColor = UIColor(red: 0.23, green: 0.58, blue: 0.20, alpha: 1.0)
        ccButton.layer.cornerRadius = 0.5 * ccButton.bounds.size.width
        ccButton.layer.borderWidth = 3.0
        ccButton.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        ccButton.clipsToBounds = true
        
        
        //cButton.backgroundColor = UIColor(red: 0.23, green: 0.58, blue: 0.20, alpha: 1.0)
        cButton.layer.cornerRadius = 0.5 * ccButton.bounds.size.width
        cButton.layer.borderWidth = 3
        cButton.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        //ctButton.backgroundColor = UIColor(red: 0.23, green: 0.58, blue: 0.20, alpha: 1.0)
        ctButton.layer.cornerRadius = 0.5 * ctButton.bounds.size.width
        ctButton.layer.borderWidth = 3
        ctButton.layer.borderColor = UIColor.groupTableViewBackground.cgColor

    }


}

//
//  CashContributionView.swift
//  LiveFunding
//
//  Created by Steven Roseman on 6/25/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import UIKit
import Firebase

class CashContributionView: UIViewController {

    @IBOutlet var three: UIButton!
    @IBOutlet var two: UIButton!
    @IBOutlet var one: UIButton!
    @IBOutlet var amountView: UILabel!
    @IBOutlet var paymentTextfield: UITextField!
    var total:String!
    var totalArray = [String]()
    var decimal:String!
    var isClicked:Bool!
    var count:Int!
    var decimalCount:Int!
    var cashContribution:String!
    var contributionTotal:String!
    var cardDetails = [CreditCardInfo]()
    var ref:FIRDatabaseReference!
    var isCashTransaction:Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        for card in cardDetails{
            isCashTransaction = card.isCardTransaction
        }

        total = ""
        isClicked = false
        count = 0
        decimal = "."
        decimalCount = 0
        
        nav(name: "Live Funding")

    }

    @IBAction func oneButtonTapped(_ sender: Any) {
        let one = "1"
        dataGrabbed(num: one)
    }
    
    
    @IBAction func twoButtonTapped(_ sender: Any) {
        let two = "2"
        
        dataGrabbed(num: two)
    }
    
    @IBAction func threeButtonTapped(_ sender: Any) {
        
        let three = "3"
        dataGrabbed(num: three)
    }
    
    @IBAction func fourButtonTapped(_ sender: Any) {
        
       let four = "4"
        dataGrabbed(num: four)
    }
    
    @IBAction func fiveButtonTapped(_ sender: Any) {
        
       let five = "5"
        dataGrabbed(num: five)
    }
    
    @IBAction func sixButtonTapped(_ sender: Any) {
        
        let six = "6"
        dataGrabbed(num: six)
    }
   
    @IBAction func sevenButtonTapped(_ sender: Any) {
        
      let seven = "7"
        dataGrabbed(num: seven)
    }
    
    @IBAction func eightButtonTapped(_ sender: Any) {
        
        let eight = "8"
        dataGrabbed(num: eight)
    }
    
    @IBAction func nineButtonTapped(_ sender: Any) {
        
        let nine = "9"
        dataGrabbed(num: nine)
    }
    
    @IBAction func zeroButtonTapped(_ sender: Any) {
        
        if amountView.text == "" {
            
        } else {
            
            let zero = "0"
            dataGrabbed(num: zero)
        }
    }
    
    @IBAction func reviewPayment(_ sender: Any) {

 
        print(total)
        if total == ""{
            return
        }

         //set bool if user is doing cash
        for details in cardDetails{
            if details.isCardTransaction == true {
                //pass card detilas
                changeView(contribution: total, details: cardDetails)
                
            } else {
               
                
                //this is cash transaction
                changeView(contribution: total, details: cardDetails)
            }
        }

    }
    
    @IBAction func decimalButtonTapped(_ sender: Any) {
       
        decimal = "."
        if(amountView.text?.isEmpty)!{
            //so decimal does not first character
        } else {
            if !total.contains(decimal){
                isClicked = false
            }
            if isClicked == false {
                
                if total.contains(decimal){
                   print("contains decimal")
                } else{
                    count = count + 1
                    
                    total = total + decimal
                    totalArray.append(total)
                    isClicked = true
                    
                    for allTotal in totalArray{
                        
                        total = allTotal
                        amountView.text = total
                    }

                }
               
            }
         
        }
        
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        print(totalArray)
        
        if totalArray.count == 1{
            totalArray.remove(at: 0)
            total = ""
            count = 0
            decimalCount = 0
           amountView.text = "$" + total
            
            return
        }
        //decrease count
        if count <= 0{
            //do nothing
            print("do nothing count")
        } else {
           count = count - 1
            print(count)
        }
        
//        if decimalCount <= 0 {
//            //do nothing
//            print("do nothing")
//        } else {
//            decimalCount = decimalCount - 1
//        }

        if totalArray.isEmpty{
            print("empty")
            total = ""
            amountView.text = ""

            
        } else {
            totalArray.removeLast()
            
            for allTotal in totalArray{

                if !allTotal.contains(decimal){
                    
                    isClicked = false
                    
                }
                total = allTotal
                amountView.text = "$" + total
            }

        }
        
    }
    
    func changeView(contribution:String, details:[CreditCardInfo] ){

        
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ReviewView") as! ReviewView
        controller.cardDetails = details
        controller.contribution = contribution
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        appDelegate.window?.rootViewController = controllerNav
    }
    
    func dataGrabbed(num:String){
        if (amountView.text?.isEmpty)! {
            total = num
            totalArray.append(total)
            amountView.text =  "$" + total
            count = count + 1
           
        } else {
            
            //move decimal count before count conditional
            if total.contains(decimal){
                
                if decimalCount >= 2{
                    
                    return
                    
                } else {
                    
                    total = total + num
                    decimalCount = decimalCount + 1
                    count = count + 1
                    totalArray.append(total)
                    for allTotal in totalArray{
                        amountView.text = "$" + allTotal
                        
                    }
                    return
                }
            }
            
            if count >= 4{
                print("no more characters")
                
            } else {
                count = count + 1
                
                total = total + num
               
                
                totalArray.append(total)
                for allTotal in totalArray{
                    amountView.text = "$" + allTotal
                    
                }
            }
            
            
        }
        
    }

    @IBAction func backToLandingViewButtonTapped(_ sender: Any) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageView") as! LandingPageView
        
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
    }
   

}

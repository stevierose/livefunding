//
//  ExtensionHelper.swift
//  LiveFunding
//
//  Created by Steven Roseman on 10/25/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func nav(name:String){
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.05, green: 0.71, blue: 0.22, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.05, green: 0.71, blue: 0.22, alpha: 1.0)
            
            nav.topItem?.title = name
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
            
            
        }
        
    }
}

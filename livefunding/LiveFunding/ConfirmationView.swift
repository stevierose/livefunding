//
//  ConfirmationView.swift
//  LiveFunding
//
//  Created by Steven Roseman on 6/29/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class ConfirmationView: UIViewController {
    
    var confirmationNumber:String!
    var confirmationAmount:String!
    var phoneNumber:String!
    var contributorName:String!
    var eventName:String!
    var transType:String!
    var ref:FIRDatabaseReference!
    var cardDetails = [CreditCardInfo]()
    var isCashTransaction:Bool!
    var counter:Int!
    var defaults = UserDefaults.standard
   
    @IBOutlet var confirmCompleted: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        nav(name: "Live Funding")
        
        
      
        
        confirmCompleted.backgroundColor = UIColor(red: 0.41, green: 0.74, blue: 0.27, alpha: 1.0)
        confirmCompleted.layer.cornerRadius = 25
        confirmCompleted.layer.borderWidth = 1
        confirmCompleted.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        for card in cardDetails{
            eventName = card.event
            if let contributorsName = card.cardUsersName{
                contributorName = contributorsName
            }
            
            
        }
       ref = FIRDatabase.database().reference().child(eventName)
        ref.child(eventName).queryOrderedByKey().observe(.childAdded, with: { (snapshot) in
            for (key, value) in (snapshot.value as? [String:AnyObject])!{
                if key == "counter"{
                    print(value)
                    self.counter = value as? Int
                }
            }
        })
        if counter == nil{
            counter = 0
        }

    }

   
    @IBAction func doneButtonTapped(_ sender: Any) {
         //add total phoneNum count confirmNum to database
        addContributor()
        
        
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageView") as! LandingPageView
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
    }

    func addContributor() {
        let key = ref.childByAutoId().key
        counter = 1

       
        let contributor = ["id":key,
                           "contributorName": contributorName,
                           "contributon": confirmationAmount,
                           "contributorPhoneNum": phoneNumber,
                           "eventName": eventName,
                           "counter":counter] as [String : Any]
        
        ref.child(key).setValue(contributor)

    }



}

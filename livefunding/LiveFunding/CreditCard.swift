//
//  CreditCard.swift
//  LiveFunding
//
//  Created by Steven Roseman on 12/9/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import Foundation

class CreditCard {
    
    var ccNum:String!
    var redactedCCNum:String!
    var expMonth:Int!
    var expYear:Int!
    var cvvNum:Int!
    var event:String!
    var isCardTransaction:Bool = false
    var cardUsersName:String!
    var cardImg:UIImage!
    
    init(ccNum:String, redactedCCNum: String, expMonth: Int, expYear: Int, cvvNum: Int, event: String,
         isCardTransaction: Bool, cardUsersName: String, cardImg: UIImage) {
        self.ccNum = ccNum
        self.redactedCCNum = redactedCCNum
        self.expMonth = expMonth
        self.expYear = expYear
        self.cvvNum = cvvNum
        self.event = event
        self.isCardTransaction = isCardTransaction
        self.cardUsersName = cardUsersName
        self.cardImg = cardImg
        
        
    }
}

class CreditCardViewModel{
    private var creditCard: CreditCard?
    
    var ccNumText: String? {
        return creditCard?.ccNum
    }
    var redactedCCNumText: String? {
        return creditCard?.redactedCCNum
    }
    var expMonthText: Int? {
        return creditCard?.expMonth
    }
    var expYearText: Int? {
        return creditCard?.expYear
    }
    var cvvNumText: Int? {
        return creditCard?.cvvNum
    }
    var eventText: String? {
        return creditCard?.event
    }
    var isCardTransactionConfirmed: Bool? {
        return creditCard?.isCardTransaction
    }
    var cardUsersNameText: String? {
        return creditCard?.cardUsersName
    }
    var cardImgPhoto: UIImage? {
        return creditCard?.cardImg
    }
    
    init(creditCard: CreditCard){
        self.creditCard = creditCard
    }
}

//
//  ReviewView.swift
//  LiveFunding
//
//  Created by Steven Roseman on 6/30/17.
//  Copyright © 2017 crosscodeapps. All rights reserved.
//

import UIKit
import Stripe
import Alamofire
import Firebase
import MessageUI

class ReviewView: UIViewController, UITextFieldDelegate, MFMessageComposeViewControllerDelegate {
    var cardDetails = [CreditCardInfo]()
    var contribution:String!
    @IBOutlet var ccNumberLabel: UILabel!
    @IBOutlet var contributionAmount: UILabel!
    @IBOutlet var monthLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var transType: UILabel!
    @IBOutlet var processButton: UIButton!
    @IBOutlet var phoneNumText: UILabel!
    @IBOutlet var phonePadView: UIView!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var phoneNumberTextfield: UITextField!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var usersListedName: UILabel!
    @IBOutlet var cardImage: UIImageView!
    
    var counter:Int!
    var cardNumber:String!
    var cardCVC:String!
    var cardExpMonth:String!
    var cardExpYear:String!
    var creditDetails:Bool!
    var cardUsersName:String!
    var eventName:String!
    var transactionType:String!
    var phoneNumArray = [String]()
    var count:Int!
    var total:String!
    var entirePhoneNum:String!
    var dashCount:Int!
    var textMessageNum:String!
    var isCardTransaction:Bool!
    var transactionContribution:String!
    var myCard:String!
    var ref:FIRDatabaseReference!
    

    override func viewDidLoad() {
        super.viewDidLoad()
      
        nav(name: "Review")
        

          isCardTransaction = false


        count = 0
        total = ""
        dashCount = 0



        self.hideKeyboardWhenTappedAround()
       transactionContribution = contribution
        
        for details in cardDetails{
            eventName = details.event
          creditDetails = details.isCardTransaction

            if details.isCardTransaction == false {

                nameTextField.isHidden = false
                cardImage.isHidden = true
                
                
            } else {
                nameTextField.isHidden = true
                cardImage.image = details.cardImg
                isCardTransaction = true
                cardNumber = details.creditCardNum
                cardExpMonth = String(details.expMonth)
                cardExpYear = String(details.expYear)
                self.cardUsersName = details.cardUsersName

                
                
            }
            
        }
        if contribution.characters.count == 7{
            contribution.insert(",", at: contribution.index(contribution.startIndex, offsetBy: 1))
            print(contribution)
        } else if contribution.characters.count == 8{
            contribution.insert(",", at: contribution.index(contribution.startIndex, offsetBy: 2))
            print(contribution)
        } else if contribution.characters.count == 9{
            
        }

        self.contributionAmount.text = "$" + contribution
    }


    @IBAction func backButtonTapped(_ sender: Any) {
        total = ""
//        phoneNumText.text = ""
//        entirePhoneNum = ""
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CashContributionView") as! CashContributionView
        controller.cardDetails = cardDetails
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = controllerNav
       
    }
    
    func handleError(error: NSError){
        let alert = UIAlertController(title: "Please try again", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        cardNumber = ""
    }
    
    func postStripeToken(token: STPToken){
        
        let URL = "http://ftp.crosscodeapps.com/payment.php"

        let params: Parameters = ["stripeToken": token.tokenId as NSObject, "amount": transactionContribution, "currency": "usd" as NSObject, "description": eventName, "customer": self.cardUsersName]
        
        
        
        Alamofire.request(URL, method: .post, parameters: params, encoding: URLEncoding.httpBody).responseJSON { response in

            
            if let data = response.data {
                let json = String(data: data, encoding: String.Encoding.utf8)
               print(json!)
                if (json?.contains("Success"))!{
                    self.entirePhoneNum = self.phoneNumberTextfield.text

                    self.contributionAlert(name:self.cardUsersName)
                    
                } else {
                  
                    let alertController = UIAlertController(title: "Unsuccessful", message: "Payment didnt process", preferredStyle: UIAlertControllerStyle.alert)
                    let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        
                        self.contribution = ""
                        self.phoneNumText.text = ""
                        self.cardUsersName = ""
                        self.cardNumber = ""
                        
                        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageView") as! LandingPageView
                        let controllerNav = UINavigationController(rootViewController: controller)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        
                        appDelegate.window?.rootViewController = controllerNav
                    })
                    alertController.addAction(alertAction)
                    self.present(alertController, animated: true, completion: nil)
                }

        }
        }

    }

    func dataGrabbed(num:String){
        let dash = "-"
        count = count + 1

        if phoneNumArray.count == 3 && !total.contains("-"){
            total = total + dash + num
            phoneNumArray.append(total)
            dashCount = dashCount + 1
        } else if phoneNumArray.count == 6 && dashCount == 1{
            total = total + dash + num
            phoneNumArray.append(total)
            dashCount = dashCount + 1
            } else
        {             total = total + num
            phoneNumArray.append(total)
        }
        if phoneNumArray.count == 11{
            phoneNumArray.removeLast()
            return
        } else if phoneNumArray.count == 8 && dashCount == 1{
            total.insert("-", at: total.index(total.startIndex, offsetBy: 7))
           
            phoneNumArray.append(total)
            dashCount = dashCount + 1
        } else if count == 4 && dashCount == 10{
            total.insert("-", at: total.index(total.startIndex, offsetBy: 3))
            phoneNumArray.append(total)
            dashCount = dashCount + 1
        }
        
        for phoneNum in phoneNumArray{
            entirePhoneNum = phoneNum
            print("number two \(entirePhoneNum)")
        }
        phoneNumText.text = entirePhoneNum
        print(dashCount)
    }

    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        if (phoneNumberTextfield.text?.count)! < 10 || (phoneNumberTextfield.text?.count)! > 10{
            //place alert number is not correct
            let alertController = UIAlertController(title: "Please add your phone number", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                
            })
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
            return
        }
        
        if nameTextField.text == ""{
            nameTextField.text = "name not provided"
        }
        
        if isCardTransaction == true {
            let stripeCard = STPCardParams()
            stripeCard.number = cardNumber
            stripeCard.cvc = cardCVC
            stripeCard.expMonth = UInt(Int(cardExpMonth)!)
            stripeCard.expYear = UInt(Int(cardExpYear)!)
            
            if STPCardValidator.validationState(forCard: stripeCard) == .valid{
                STPAPIClient.shared().createToken(withCard: stripeCard) { (token, error) in
                    if error != nil{
                        //alert error
                        self.handleError(error: error! as NSError)
                        return
                    }
                    print(token!)
                    self.postStripeToken(token: token!)
                }
                
            } else {
                let alert = UIAlertController(title: "Credit card not valid", message: "Reenter", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        } else {
           
            entirePhoneNum = phoneNumberTextfield.text
            
            contributionAlert(name:self.nameTextField.text!)

        }
        
    }
    
    func contributionAlert(name:String){
        let alertController = UIAlertController(title: "Thank you", message: "We Appreciate your contribuition", preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            self.addContributor(name:name)
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageView") as! LandingPageView
            let controllerNav = UINavigationController(rootViewController: controller)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.window?.rootViewController = controllerNav
        })
        
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    func addContributor(name:String) {
        ref = FIRDatabase.database().reference().child(eventName)
        ref.child(eventName).queryOrderedByKey().observe(.childAdded, with: { (snapshot) in
            for (key, value) in (snapshot.value as? [String:AnyObject])!{
                if key == "counter"{
                    print(value)
                    self.counter = value as? Int
                }
            }
        })
        if counter == nil{
            counter = 0
        }
        let key = ref.childByAutoId().key
        counter = 1
        
        
        let contributor = ["id":key,
                           "contributorName": name,
                           "contributon": contribution,
                           "contributorPhoneNum": entirePhoneNum,
                           "eventName": eventName,
                           "counter":counter] as [String : Any]
        
        ref.child(key).setValue(contributor)
        
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }

}
